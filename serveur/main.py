from typing import Optional

from fastapi import FastAPI
import requests
import json
import uvicorn
from mot import Mot  
from guess import Guess    
       
app = FastAPI()

def getMots():
    return ["arbre","maison","voiture"]

@app.get("/init")
def read_mots():
    return {"mots" : getMots()}

@app.get("/mots")
def read_mots():
    return {"mots" : getMots()}

@app.post("/mot")
def add_mot(mot: Mot):
    return mot

@app.post("/guess")
def add_guess(guess: Guess):
    if Guess.letter not in Guess.mot.caracteres:
       Guess.erreurs +=1
         
if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
    
