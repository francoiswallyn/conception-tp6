import requests
from serveur.mot import Mot
from dotenv import load_dotenv
import json
import os
load_dotenv()
url = os.environ.get("URL")
def get_mots():
    requete = requests.get(f"{url}/mots")
    info = requete.json()
    return info

def add_mot(mot:Mot):
    requests.post("http://127.0.0.1:8000/mot", data=mot)